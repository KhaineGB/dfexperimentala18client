using Harmony;
using System;
using System.Collections.Generic;
using System.Globalization;
using Audio;
using UnityEngine;

public class Khaine_StamFloattoInt
{
    [HarmonyPatch(typeof(XUiC_HUDStatBar))]
    [HarmonyPatch("GetBindingValue")]
	public class Khaine_GetBindingValuePatch
	{
		//triple underscore allows access to private stuff without patchscripts when in same class
		//instance replaces this but when something with triple underscores needs to go after it then instance needs to be removed
		//cachedstringformatter doesnt use ref but still needs a triple underscore cos private
		static void Postfix (XUiC_HUDStatBar __instance, ref string value, BindingItem binding, ref HUDStatGroups ___statGroup, ref HUDStatTypes ___statType, ref CachedStringFormatter<int> ___statcurrentFormatterInt) 
		{
			string fieldName = binding.FieldName;
			if ( fieldName == "statcurrent" )
			{
				if (__instance.LocalPlayer == null || (___statGroup == HUDStatGroups.Vehicle && __instance.Vehicle == null))
					{
						value = "";
						return; //this not returning true is fine since we're using a void
					}
					switch (___statType)
					{
						case HUDStatTypes.Stamina:
						value = ___statcurrentFormatterInt.Format((int)__instance.LocalPlayer.Stamina);
						break;
					}
			}
		}
	}
}